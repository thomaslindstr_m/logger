const isFunction = require('@amphibian/is-function');
const map = require('@amphibian/map');

/**
 * Prepare logs for output
 * @param {array} logs
 * @returns {array}
**/
function prepareLogs(logs) {
    return map(logs, (log) => {
        if (isFunction(log)) {
            return log();
        }

        return log;
    });
}

/**
 * Create logger
 * @param {string} prefix
 * @returns {function} logger
**/
function createLogger(...prefix) {
    return {
        isEnabled: true,
        log(...suffix) {
            if (this.isEnabled) {
                console.log.apply(
                    null,
                    prepareLogs(prefix.concat('›', ...suffix))
                );
            }

            return this;
        },
        extend(...suffix) {
            const child = createLogger.apply(createLogger, prefix.concat(suffix));

            if (!this.isEnabled) {
                child.disable();
            }

            return child;
        },
        enable() {
            this.isEnabled = true;
            return this;
        },
        disable() {
            this.isEnabled = false;
            return this;
        }
    };
}

const timeLogger = createLogger(() => `[${new Date().toISOString()}]`);

exports.default = createLogger;
module.exports = {createLogger, timeLogger};
