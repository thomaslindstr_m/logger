# logger

[![build status](https://gitlab.com/thomaslindstr_m/logger/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/logger/commits/master)

pretty, extendable logs

```
npm install @amphibian/logger
```

```javascript
var chalk = require('chalk');
var {createLogger, timeLogger} = require('@amphibian/logger');

timeLogger.log('hello'); // > [2017-04-29T11:15:32.596Z] › hello

// Create your own logs
var logger = createLogger(() => (
    chalk.grey('[my-project]')
));

logger.log('Something!'); // > [my-project] › Something!

// Extend logs
var helloLogger = timeLogger.extend('hello');
helloLogger.log('you!'); // > [2017-04-29T11:15:32.596Z] hello › you

// Disable/enable logs
helloLogger.disable();
helloLogger.log('something'); // (nothing)
helloLogger.extend('you').log('there'); // (nothing)
helloLogger.enable();
helloLogger.log('hello!');
```
