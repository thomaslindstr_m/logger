const {createLogger, timeLogger} = require('./index.js');

describe('logger', () => {
    it('should return log function', () => {
        const testLogger = createLogger('something');
        testLogger.log('hello 1');
    });

    it('should return extend function', () => {
        const testLogger = createLogger('something');
        testLogger.extend('hello 2');
    });

    it('log should return the logger object', () => {
        const testLogger = createLogger('something');
        testLogger.log('hello').log('hello 3').extend('hi');
    });

    it('extend should return the logger object', () => {
        const testLogger = createLogger('something');
        testLogger.extend('hello').extend('hello 4').log('hi');
    });

    it('should call functions', () => {
        let didCall = false;
        const testLogger = createLogger('something');

        testLogger.log(() => {
            didCall = true;
            return 'logged';
        });

        if (!didCall) {
            throw new Error('did not call');
        }
    });

    it('should enable logging', () => {
        const testLogger = createLogger('something');
        testLogger.enable();
    });

    it('should disable logging', () => {
        let didLog = false;
        const testLogger = createLogger('something');

        testLogger.disable();
        testLogger.log(() => {
            didLog = true;
            return 'logged 1';
        });

        if (didLog) {
            throw new Error('logged');
        }
    });

    it('should enable logging after disabling it', () => {
        let didLog = false;
        const testLogger = createLogger('something');

        testLogger.disable();
        testLogger.log(() => {
            didLog = true;
            return 'logged 2';
        });

        if (didLog) {
            throw new Error('logged');
        }

        testLogger.enable();
        testLogger.log(() => {
            didLog = true;
            return 'logged 3';
        });

        if (!didLog) {
            throw new Error('did not log');
        }
    });

    it('should inherit enabled/disabled when extending', () => {
        let didLog = false;

        const testLogger = createLogger('something');
        testLogger.disable();

        const childLogger = testLogger.extend('else');

        childLogger.log(() => {
            didLog = true;
            return 'logged 4';
        });

        if (didLog) {
            throw new Error('did log');
        }
    });

    it('should export timeLogger', () => {
        timeLogger.log('hello');
    });
});
